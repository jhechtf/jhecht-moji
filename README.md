# Jhechtmoji

Jhechtmoji is a configurable Git Emoji package. It ships with a default setup of [Emoji Commit Messages](https://gist.github.com/parmentf/035de27d6ed1dce0b36a?fbclid=IwAR3CbKdhrzA4fML-m3HhG7diwmggDfp2YBKkkJ7eKkdQof0X0C_uP6wPSiU), but can set to use any emoji. This makes Jhechtmoji an ideal extension for teams that want to use a standard Emoji Map between them, without needing to make their own extension or be limited by other similar extensions.

## Features

This extension aims to keep things simple. As such, unlike other Git extensions, Jhechtmoji doesn't reinvent the wheel &ndash; it uses the API exposed by the VSCode Git extension, along with some simple configuration and minimal UI. 

## Requirements

This extension requires [node-emoji](https://www.npmjs.com/package/node-emoji) to convert from emoji shortcodes to their unicode values.

## Extension Settings

Jhechtmoji utilizes two user settings `jhechtmoji.convertToUnicode` and `jhechtmoji.emojiMap`.

`jhechtmoji.convertToUnicode` is set to `true` by default; this means that each time you use the `jhechtmoji.commit` command it will turn the shortcode, e.g. `:tada:` and replace it with the unicode value instead, 🎉 in this case.

`jhechtmoji.emojiMap` is a map of `"[Descriptor of emoji]": ":emoji_shortcode:"` values. This value can be updated to reflect your personal use of Emojis in Git, or to reflect a standardized Git Emoji setup between a development team. 


## Known Issues

1. In repositories with submodules the extension is currently unable to highlight the correct input box after selecting a message type. The UI has been updated, but unfortunately
there is not a way to fix this issue at this time.

## Release Notes

Release notes moved to CHANGELOG.md

## Other

If you like Angular's commit style, but also want to add some extra zing into your commits, I've created the following rough map you can use for your `jhechtmoji.emojiMap` value. This would basically mean that you sub out the `fix`, `feat`, etc. type notification for the given emoji. If you plan on using your commits with a tool that generates your CHANGELOG.md for you, then I recommend setting `jhechtmoji.convertToUnicode` to false so that you can more easily check the value.

```json
{
  "build": ":package:",
  "ci": ":green_heart:",
  "docs": ":books:",
  "feat": ":sparkles:",
  "fix": ":bug:",
  "perf": ":racehorse:",
  "test": ":white_check_mark:",
  "style": ":lipstick:",
}
```

This is obviously just my mapping, you are free to use or change any you feel are out of line with the Angular commit style.
import { extensions, window, workspace, commands, ExtensionContext, QuickPickItem } from 'vscode';
import { emojify } from 'node-emoji';
// import * as cp from 'child_process';
import { GitExtension, Repository } from './git';

export function activate(context: ExtensionContext): void {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
	const git = extensions.getExtension<GitExtension>('vscode.git')!.exports;
	// Check to make sure the git extension is enabled. Otherwise the rest of this is sort of for naught
	if (!git.enabled) {
		// Show some errors otherwise.
		console.error('Cannot use Jhechtmoji without the VSCode Git Extension');
		window.showErrorMessage('Cannot use Jhechtmoji with VSCode Git Extension', { modal: true });
		return;
	}
	// get the actual gitAPI exposed by the extension
	const gitApi = git.getAPI(1);
	// get this extensions configuration objects.
	let jhechtmoji = workspace.getConfiguration('jhechtmoji');
	const emojisMap = jhechtmoji.get('emojiMap') as { [key: string]: string };
	const convertToUnicode = jhechtmoji.get('convertToUnicode');
	const emojis = Object.entries(emojisMap).map(([key, value]): QuickPickItem => {
		return {
			label: emojify(value),
			detail: key,
			description: value
		};
	});
	// push our command to the context subscriptions
	context.subscriptions.push(commands.registerCommand('jhechtmoji.commit', async () => {
		// get our emoji.
		const emoji = await window.showQuickPick(emojis, { placeHolder: 'Start typing to search for an emoji or description', matchOnDetail: true, matchOnDescription: true });
		// no emoji, no dice.
		if (!emoji) {
			console.error('No emoji chosen');
			return null;
		}
		// repo is either going to be a repository or undefined
		let repo: Repository | undefined;
		// If there are multiple repositories then you should choose one.
		// Not choosing one defaults to whatever the repository at index 0 will be.
		if (gitApi.repositories.length > 1) {
			repo = await window.showQuickPick(gitApi.repositories.map((r, index) => ({
				label: r.rootUri.path,
				index
			})), {
				placeHolder: 'Which repository is this commit for?'
			}).then((r) => r ? gitApi.repositories[r.index] : gitApi.repositories[0]);
		} else {
			repo = gitApi.repositories[0];
		}
		repo.inputBox.value = (convertToUnicode ? emoji.label : emoji.detail) + ' ';
		// highlight the SCM panel
		commands.executeCommand('workbench.view.scm');
	}));
	// run this to check if our configuration changes.
	context.subscriptions.push(
		workspace.onDidChangeConfiguration((e) => {
			// check if this affects our configurations
			if (e.affectsConfiguration('jhechtmoji')) {
				// update the configuration
				jhechtmoji = workspace.getConfiguration('jhechtmoji');
			}
		})
	);
}

// this method is called when your extension is deactivated
/* eslint-disable-next-line @typescript-eslint/no-empty-function */
export function deactivate(): void { }

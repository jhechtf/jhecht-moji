# Contributing to Jhechtmoji

I don't imagine anyone would want to contribute to this random extension, but I'll add this in just in case.

## Notes

I'll likely be moving the structure of the repository around just to make it easier to work with later on, _if_ I decide to add in other features. Not to say I will, just to say that I might. I am debating reaching out to the Gitlens team to see if I could perhaps add in some API to communicate between the two extensions (if they're both installed). 

I will state that I am open to Feature Requests, but I do want to keep the extension fairly simple. It is literally just a way to allow people and teams to use a customized version of Git Emojis. I think most of the other Git functionality is already coded into the Git extension, or into Gitlens. 

## Features

Please create a new issue on the Gitlab repository at https://gitlab.com/jhechtf/jhecht-moji and start with with `[Feature Request] Quick description of your feature request`. Please try to make any feature requests as clear as possible, and give a use case that you would like for the extension. I'll read through it and comment on whether or not I think the issue fits with where I see "Jhechtmoji" (which is honestly an awful name, I know, but it was all I could come up with) going towards.

## Issues / Bugs

If you find a Bug when using my extension, _please_ let me know! "Please create an issue and title it `[Bug] Short description of the bug`. In that, please add in your VSCode version, what version of the extension you are using, and any other data you feel may be relevant (OS and such is appreciated, but likely not needed). I'll go through afterwards and either tag is as reproducible and attempt a fix, or I'll tag it as not reproducible and ask for you to give more information.

## Other Merge Requests

If you would like to merge something into Jhechtmoji, please understand that I will have to review it before approving it. The same thing goes for features -- if it doesn't fit with where I see the extension going, I simply won't merge it in.

After forking the repo, please create a branch from `master` as it will be the main branch for this repo. I'll work on tagging releases later. Then create the merge request, and please remember to keep inline with my linter -- code that doesn't pass a lint is code I am unlikely to use.

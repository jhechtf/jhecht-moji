# Basic Info

**VSCode Version**: (Can be grabbed from Help > About)
**Operating System**: Windows / Mac / Linux (if Linux, please include the distro such as Ubuntu, Fedora, etc.)
**Jhecht-moji Version**: 

# Description of Bug / Issue

<!-- Please be as clear and concise as possible. -->

# Steps to reproduction

<!-- Any relevant steps you must take in order to reproduce this issue. -->
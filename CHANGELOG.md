# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.1](https://gitlab.com/jhechtf/jhecht-moji/compare/v0.1.0...v1.0.1) (2019-11-04)


### Miscellaneous

- Added in linter script ([f80a36e](https://gitlab.com/jhechtf/jhecht-moji/commit/f80a36e2e03c9b6a24098574b6a87eae2fe3ce4c))
- Added in automatic deployment for the extension. ([39a0f93](https://gitlab.com/jhechtf/jhecht-moji/commit/39a0f936f3f132917a616bd7ddc3047f9b7e2e04)), closes [#1](https://gitlab.com/jhechtf/jhecht-moji/issues/1/
- Fixed misspelled iamge name ([8014fb0](https://gitlab.com/jhechtf/jhecht-moji/commit/8014fb03306b4bc3fed688f559157def006933f7))
- Merge branch 'ci/fix-image-name' into 'master' ([a7b4551](https://gitlab.com/jhechtf/jhecht-moji/commit/a7b45519fb3e390b6df4ca63ea10fbaab5329897))
- Merge branch 'feat/add-auto-deploy' into 'master' ([0bf267b](https://gitlab.com/jhechtf/jhecht-moji/commit/0bf267bf44f9687d145db0f4ba7d70e011766e45)), closes [#1](https://gitlab.com/jhechtf/jhecht-moji/issues/1/
- Merge branch 'lint/add-in-linter-support' into 'master' ([e427a0d](https://gitlab.com/jhechtf/jhecht-moji/commit/e427a0df6b55858c593c78e1ed07531b39ab21e6))

## [0.1.0]

### General

1. Rewrote the code to make use of async/await in TypeScript. This involved some minor bumps to depenency versions as well as upgrading to using the new `vscode-test` package.
2. Removed the Release Notes section from the README.md file. Changelog will be the release notes, especially since most commits will tend to be small.
3. Changed settings scope from "application" to "resource", allowing for settings to be stored inside `.vscode/settings.json`
4. Moving up to a minor release, 0.1.0. 
5. Added in a mapping between Angular's commit style and my base Gitmoji config. Just in case you like the idea behind Angular's commit syntax, but also want to have some pizazz in the commit header.
6. Fixed typo in README.md. Line one should have read `It _ships_ ...` and not... what it said there originally.
